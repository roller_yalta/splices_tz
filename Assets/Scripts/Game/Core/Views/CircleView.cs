﻿using UnityEngine;

public class CircleView : MonoBehaviour
{
    [SerializeField] private ParticleSystem explosionParticle;
    private Animation animation;

    private void Awake()
    {
        animation = GetComponent<Animation>();
    }

    public void Explode()
    {
        explosionParticle.Play();
    }

    public void PlayErrorAnimation()
    {
        animation.Play();
    }
}