﻿using UnityEngine.Events;

public class GameEvent
{
    public UnityAction Event;

    public void AddListener(UnityAction action)
    {
        Event += action;
    }

    public void RemoveListener(UnityAction action)
    {
        Event -= action;
    }

    public void Invoke()
    {
        Event?.Invoke();
    }
}

public class GameEvent<T>
{
    public UnityAction<T> Event;

    public void AddListener(UnityAction<T> action)
    {
        Event += action;
    }

    public void RemoveListener(UnityAction<T> action)
    {
        Event -= action;
    }

    public void Invoke(T value)
    {
        Event?.Invoke(value);
    }
}
