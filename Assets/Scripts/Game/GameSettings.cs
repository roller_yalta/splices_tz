﻿using UnityEngine;

[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    [Range(0, 100)] public int PointsPerCircle;
    [Range(0f, 1f)] public float PieceMoveTiming;

    [Header("TimerBar speed")] [Range(0f, 10f)]
    public float TimerBarSpeed;
}