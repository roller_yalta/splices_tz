﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CircleView))]
public class Circle : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private int index;
    private Slot[] slots = new Slot[6];
    private List<Piece> pieces = new List<Piece>();
    private CircleView view;

    private void Awake()
    {
        InitSlots();
        view = GetComponent<CircleView>();
    }

    private void OnEnable()
    {
        EventHolder.PieceCreated.AddListener(OnPieceCreated);
        EventHolder.PieceHasCome.AddListener(OnPieceHasCome);
        EventHolder.CircleFinished.AddListener(OnCircleFinished);
    }

    private void OnDisable()
    {
        EventHolder.PieceCreated.RemoveListener(OnPieceCreated);
        EventHolder.PieceHasCome.RemoveListener(OnPieceHasCome);
        EventHolder.CircleFinished.RemoveListener(OnCircleFinished);
    }

    private void OnPieceCreated(Piece piece)
    {
        var canFit = CanFit(piece);
        if (canFit) piece.CanBePlaced = true;
    }

    private void OnPieceHasCome()
    {
        if (slots.Any(x => x.isFree)) return;
        CleanupCircle();
        EventHolder.CircleFinished.Invoke(index);
    }

    private void CleanupCircle()
    {
        foreach (var piece in pieces)
        {
            Destroy(piece.gameObject);
        }

        if (pieces.Count > 0) view.Explode();
        pieces.Clear();
        InitSlots();
        EventHolder.PointsIncreased.Invoke(GameController.Settings.PointsPerCircle);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var piece = PieceFactory.CurrentPiece;
        if (CanFit(piece))
        {
            foreach (var slot in piece.GetAffectedSlots())
            {
                slots[slot.index].isFree = false;
            }

            pieces.Add(piece);
            piece.Move(this);
        }
        else
        {
            view.PlayErrorAnimation();
        }
    }

    private bool CanFit(Piece piece)
    {
        var affectedSlots = piece.GetAffectedSlots();
        for (var i = 0; i < affectedSlots.Count; i++)
        {
            if (slots.ElementAt(affectedSlots[i].index).isFree) continue;
            return false;
        }

        return true;
    }

    private void InitSlots()
    {
        for (var i = 0; i < slots.Length; i++)
        {
            slots[i] = new Slot()
            {
                index = i,
                isFree = true
            };
        }
    }

    private void OnCircleFinished(int circleIndex)
    {
        var upperIndex = circleIndex + 1;
        if (upperIndex >= GameConstants.CircleSectors) upperIndex = 0;
        var lowerIndex = circleIndex - 1;
        if (lowerIndex < 0) lowerIndex = GameConstants.CircleSectors - 1;

        if (index == lowerIndex || index == upperIndex) CleanupCircle();
    }
}