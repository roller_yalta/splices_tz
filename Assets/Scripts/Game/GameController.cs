﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class GameController : MonoBehaviour
{
    public static GameSettings Settings;

    [SerializeField] private GameObject gameOverPanel;

    private void Awake()
    {
        Addressables.LoadAsset<GameSettings>(GameConstants.GameSettingsAdress).Completed += OnCompleted;
    }

    private void OnCompleted(AsyncOperationHandle<GameSettings> obj)
    {
        Settings = obj.Result;
    }

    private void OnEnable()
    {
        EventHolder.GameOver.AddListener(OnGameOver);
    }

    private void OnDisable()
    {
        EventHolder.GameOver.RemoveListener(OnGameOver);
    }

    private void OnGameOver()
    {
        gameOverPanel.SetActive(true);
    }
}