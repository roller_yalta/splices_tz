﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(PIeceView))]
public class Piece : MonoBehaviour, IPiece
{
    [HideInInspector] public bool CanBePlaced;
    private List<Slot> affectedSlots = new List<Slot>();
    private PIeceView view;
    private float checkDelay = .1f;

    public void Initialize(int size, int rootSlot)
    {
        for (var i = 0; i < size; i++)
        {
            var slot = new Slot() {index = rootSlot + i, isFree = false};
            if (slot.index >= GameConstants.CircleSectors) slot.index = slot.index - GameConstants.CircleSectors;
            affectedSlots.Add(slot);
            view = GetComponent<PIeceView>();
            view.CreateView(slot);
        }

        EventHolder.PieceCreated.Invoke(this);
        StartCoroutine(CheckGameOver(this));
    }

    public void Move(Circle circle)
    {
        transform.SetParent(circle.transform);
        transform.DOLocalMove(Vector3.zero, GameController.Settings.PieceMoveTiming).onComplete +=
            () => { EventHolder.PieceHasCome.Invoke(); };
    }

    public List<Slot> GetAffectedSlots()
    {
        return affectedSlots;
    }

    private IEnumerator CheckGameOver(Piece piece)
    {
        yield return new WaitForSeconds(checkDelay);
        if (!piece.CanBePlaced) EventHolder.GameOver.Invoke();
    }
}