﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreBar : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    private int score;

    private void Awake()
    {
        score = PlayerPrefs.GetInt(GameConstants.ScoreSavedValue, 0);
        scoreText.text = score.ToString();
    }

    private void OnEnable()
    {
        EventHolder.PointsIncreased.AddListener(UpdateScore);
    }

    private void OnDisable()
    {
        EventHolder.PointsIncreased.RemoveListener(UpdateScore);
    }

    private void UpdateScore(int value)
    {
        score += value;
        scoreText.text = score.ToString();

        PlayerPrefs.SetInt(GameConstants.ScoreSavedValue, score);
    }
}