﻿using UnityEngine;

[CreateAssetMenu]
public class PieceSpawnSettings : ScriptableObject
{
    [Header("Probabilities for multipieces spawning")]
    [Range(0f, 1f)] public float DoublePieceProbability;
    [Range(0f, 1f)] public float TripplePieceProbability;
}