﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPiece
{
    void Move(Circle circle);
}
