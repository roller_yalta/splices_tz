﻿using System.Collections;
using TMPro;
using UnityEngine;

public class StartPopup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI countText;
    private int counter = 3;
    private float numbersDelay = .2f;

    IEnumerator Start()
    {
        for (var i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(numbersDelay);
            counter--;
            countText.text = counter > 0 ? counter.ToString() : "GO";
        }
        
        EventHolder.StartGame.Invoke();
        gameObject.SetActive(false);
    }
}