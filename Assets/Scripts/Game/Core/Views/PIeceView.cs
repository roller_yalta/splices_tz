﻿using UnityEngine;
using UnityEngine.UI;

public class PIeceView : MonoBehaviour
{
    [SerializeField] private GameObject pieceViewPrefab;

    public void CreateView(Slot slot)
    {
        var obj = Instantiate(pieceViewPrefab, transform);
        obj.transform.position = Vector3.zero;
        var rotationValue = GameConstants.PieceBaseRotationOffset + GameConstants.PieceRotationStep * slot.index;
        obj.transform.localEulerAngles = new Vector3(0, 0, rotationValue);
    }
}