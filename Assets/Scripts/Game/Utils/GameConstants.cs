﻿
public class GameConstants
{
    public const float PieceBaseRotationOffset = 30f;
    public const float PieceRotationStep = 60f;

    public const int CircleSectors = 6;

    public const string ScoreSavedValue = "score_saved_value";

    public const string FactorySettingsAdress = "FactorySettings";
    public const string GameSettingsAdress = "GameSettings";
}