﻿using UnityEngine;
using UnityEngine.UI;

public class TimerBar : MonoBehaviour
{
    [SerializeField] private Image fillImage;
    private float fillRate;

    private void Init()
    {
        fillRate = GameController.Settings.TimerBarSpeed;
    }

    private void OnEnable()
    {
        EventHolder.StartGame.AddListener(Init);
        EventHolder.GameOver.AddListener(OnGameOver);
        EventHolder.PieceHasCome.AddListener(ResetCounter);
    }

    private void OnDisable()
    {
        EventHolder.StartGame.RemoveListener(Init);
        EventHolder.GameOver.RemoveListener(OnGameOver);
        EventHolder.PieceHasCome.RemoveListener(ResetCounter);
    }

    private void Update()
    {
        fillImage.fillAmount -= fillRate * Time.deltaTime;
        if (fillImage.fillAmount <= 0)
        {
            EventHolder.GameOver.Invoke();
        }
    }

    private void ResetCounter()
    {
        fillImage.fillAmount = 1f;
    }

    private void OnGameOver()
    {
        gameObject.SetActive(false);
    }
}