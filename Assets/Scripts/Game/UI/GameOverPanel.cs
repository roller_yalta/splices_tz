﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField] private Button replayButton;

    private void Awake()
    {
        replayButton.onClick.AddListener(Replay);
    }

    private void Replay()
    {
        SceneManager.LoadScene("game");
    }
}