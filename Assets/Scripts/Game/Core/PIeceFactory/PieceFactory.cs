﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class PieceFactory : MonoBehaviour
{
    public static Piece CurrentPiece;

    [SerializeField] private GameObject piecePrefab;
    private PieceSpawnSettings settings;
    private int prevIndex;

    private void Awake()
    {
        Addressables.LoadAsset<PieceSpawnSettings>(GameConstants.FactorySettingsAdress).Completed += OnSettingsDone;
    }

    private void OnSettingsDone(AsyncOperationHandle<PieceSpawnSettings> obj)
    {
        settings = obj.Result;
    }

    private void OnEnable()
    {
        EventHolder.PieceHasCome.AddListener(CreatePiece);
        EventHolder.StartGame.AddListener(CreatePiece);
    }

    private void OnDisable()
    {
        EventHolder.PieceHasCome.RemoveListener(CreatePiece);
        EventHolder.StartGame.RemoveListener(CreatePiece);
    }

    public void CreatePiece()
    {
        var go = Instantiate(piecePrefab, transform);
        var piece = go.GetComponent<Piece>();
        piece.Initialize(GetPiecesMultiplier(), GetSectorIndex());
        CurrentPiece = piece;
    }

    private int GetPiecesMultiplier()
    {
        var rndValue = Random.value;
        if (rndValue < settings.TripplePieceProbability) return 3;
        return rndValue < settings.DoublePieceProbability ? 2 : 1;
    }

    private int GetSectorIndex()
    {
        var index = Random.Range(0, GameConstants.CircleSectors);
        if (index == prevIndex)
        {
            while (index == prevIndex)
            {
                index = Random.Range(0, GameConstants.CircleSectors);
            }
        }

        prevIndex = index;
        return index;
    }
}