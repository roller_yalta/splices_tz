﻿
public class EventHolder
{
    public static GameEvent<Piece> PieceCreated = new GameEvent<Piece>();
    public static GameEvent PieceHasCome = new GameEvent();
    public static GameEvent<int> PointsIncreased = new GameEvent<int>();
    public static GameEvent<int> CircleFinished = new GameEvent<int>();

    public static GameEvent StartGame = new GameEvent();
    public static GameEvent GameOver = new GameEvent();
}